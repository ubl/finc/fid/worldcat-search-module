<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

use finc\WorldCat\AjaxHandler\GetWorldCat;
use finc\WorldCat\AjaxHandler\GetWorldCatFactory;
use finc\WorldCat\Client\WorldCatClient;
use finc\WorldCat\Client\WorldCatClientFactory;

return [
    'service_manager' => [
        'allow_override' => true,
        'factories' => [
            WorldCatClient::class => WorldCatClientFactory::class,
        ],
        'aliases' => [
            'finc\WorldCatClient' => WorldCatClient::class,
        ]
    ],
    'vufind' => [
        'plugin_managers' => [
            'ajaxhandler' => [
                'factories' => [
                    GetWorldCat::class => GetWorldCatFactory::class,
                ],
                'aliases' => [
                    'getWorldCat' => GetWorldCat::class,
                ]
            ]
        ]
    ]
];