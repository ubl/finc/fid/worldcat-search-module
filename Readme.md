# VuFind WorldCat Module

Introduction
------------

This module is an Open Source Software of the University Library Leipzig.
The module retrieve the data of [WorldCat Search API / Library Catalog URLs](https://www.oclc.org/developer/develop/web-services/worldcat-search-api/library-catalog-url.en.html).

Requirements
------------

Only Records with
* ISBN/ISSN
* ZDB ID

will be retrieve the worldcat data.

Installation
------------

**Composer**

Packagist Repo [Worldcat Search Module](https://packagist.org/packages/finc/worldcat-search-module)

To Install use Composer
    
    php composer require finc\worldcat-search-module
    
**VuFind**

*theme.config.php*

Write to the configuration a *mixins*-statement.

```
return [
  [...],
  'mixins' => [
    'worldcat'
  ],
  [...]
];
```

*templates/**file**.phtml*

To display the data when write the following JS-Script in to a PHTML-File

```
<? $script = <<<JS
$(document).ready(function() {
  var recordId = $('.hiddenId').val();
  var recordSource = $('.hiddenSource').val();
  $.ajax({
    dataType: 'json',
    url: VuFind.path + '/AJAX/JSON?method=getWorldCat',
    method: 'GET',
    data: {id: recordId, source: recordSource}
  }).done(function(response) {
    $('.worldcat-data').html(response.data.html);
  });
});
JS;
?>
<?=$this->inlineScript(\Laminas\View\Helper\HeadScript::SCRIPT, $script, 'SET');?>
<div class="worldcat-data"></div>
```

Configuration
-------------

You must to create a configuration file in config folder.

**worldcat.ini**
    
    [General]
    wskey=[wskey of oclc]
    serverip=[default ip]
