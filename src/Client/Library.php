<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\WorldCat\Client;

class Library
{
    /**
     * @var string
     */
    private $institutionName;

    /**
     * @var string
     */
    private $streetAddress1;

    /**
     * @var string
     */
    private $streetAddress2;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $opacUrl;

    /**
     * @var string
     */
    private $oclcSymbol;

    /**
     * @var string
     */
    private $distance;

    /**
     * @var Diagnostic|null
     */
    private $diagnostic;

    /**
     * @return string
     */
    public function getInstitutionName()
    {
        return $this->institutionName;
    }

    /**
     * @param string $institutionName
     */
    public function setInstitutionName($institutionName)
    {
        $this->institutionName = $institutionName;
    }

    /**
     * @return string
     */
    public function getStreetAddress1()
    {
        return $this->streetAddress1;
    }

    /**
     * @param string $streetAddress1
     */
    public function setStreetAddress1($streetAddress1)
    {
        $this->streetAddress1 = $streetAddress1;
    }

    /**
     * @return string
     */
    public function getStreetAddress2()
    {
        return $this->streetAddress2;
    }

    /**
     * @param string $streetAddress2
     */
    public function setStreetAddress2($streetAddress2)
    {
        $this->streetAddress2 = $streetAddress2;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getOpacUrl()
    {
        return $this->opacUrl;
    }

    /**
     * @param string $opacUrl
     */
    public function setOpacUrl($opacUrl)
    {
        $this->opacUrl = $opacUrl;
    }

    /**
     * @return string
     */
    public function getOclcSymbol()
    {
        return $this->oclcSymbol;
    }

    /**
     * @param string $oclcSymbol
     */
    public function setOclcSymbol($oclcSymbol)
    {
        $this->oclcSymbol = $oclcSymbol;
    }

    /**
     * @return string
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param string $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * @return Diagnostic|null
     */
    public function getDiagnostic()
    {
        return $this->diagnostic;
    }

    /**
     * @param Diagnostic|null $diagnostic
     */
    public function setDiagnostic(Diagnostic $diagnostic = null)
    {
        $this->diagnostic = $diagnostic;
    }
}
