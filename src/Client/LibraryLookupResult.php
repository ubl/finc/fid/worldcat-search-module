<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\WorldCat\Client;

class LibraryLookupResult
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $publisher;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string[];
     */
    private $isbns;

    /**
     * @var string
     */
    private $oclcNumber;

    /**
     * @var Library[]
     */
    private $libraries;

    /**
     * @var Diagnostic|null
     */
    private $diagnostic;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param string $publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string[]
     */
    public function getIsbns()
    {
        return $this->isbns;
    }

    /**
     * @param string[] $isbn
     */
    public function addIsbn($isbn)
    {
        $this->isbns[] = $isbn;
    }

    /**
     * @param string[] $isbns
     */
    public function setIsbns(array $isbns)
    {
        $this->isbns = $isbns;
    }

    /**
     * @return string
     */
    public function getOclcNumber()
    {
        return $this->oclcNumber;
    }

    /**
     * @param string $oclcNumber
     */
    public function setOclcNumber($oclcNumber)
    {
        $this->oclcNumber = $oclcNumber;
    }

    /**
     * @return Library[]
     */
    public function getLibraries()
    {
        return $this->libraries;
    }

    /**
     * @param Library[] $libraries
     */
    public function setLibraries(array $libraries)
    {
        $this->libraries = $libraries;
    }

    /**
     * @param Library $library
     */
    public function addLibrary(Library $library)
    {
        $this->libraries[] = $library;
    }

    /**
     * @return Diagnostic|null
     */
    public function getDiagnostic()
    {
        return $this->diagnostic;
    }

    /**
     * @param Diagnostic|null $diagnostic
     */
    public function setDiagnostic(Diagnostic $diagnostic = null)
    {
        $this->diagnostic = $diagnostic;
    }

    /**
     * @return Library[]
     */
    public function getFoundLibraries()
    {
        return array_filter($this->libraries, function (Library $lib) {
            return !$lib->getDiagnostic();
        });
    }
}
