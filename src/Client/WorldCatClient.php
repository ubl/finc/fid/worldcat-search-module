<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */
namespace finc\WorldCat\Client;

use VuFindHttp\HttpServiceInterface as HttpService;

class WorldCatClient
{
    const BASE_URI = "http://www.worldcat.org/webservices/catalog";

    /**
     * @var HttpService
     */
    private $httpService;

    /**
     * @var \JsonMapper
     */
    private $jsonMapper;
    
    /**
     * Client constructor.
     *
     * @param HttpService  $httpService
     */
    public function __construct(HttpService $httpService)
    {
        $this->httpService = $httpService;
        $this->jsonMapper = new \JsonMapper;
    }

    /**
     * @param string $uriSuffix
     *
     * @return LibraryLookupResult
     * @throws WorldCatClientException
     */
    public function lookupLibraries($uriSuffix, array $params = [])
    {
        $query = http_build_query(['format' => 'json'] + $params);
        $uri = static::BASE_URI . "/content/libraries/$uriSuffix?" .$query;

        try {
            $client = $this->httpService->createClient($uri);
            /* @var \Laminas\Http\Response $response */
            $response = $client->send();
        } catch (HttpClientException $e) {
            throw new WorldCatClientException($e->getMessage(), $e->getCode(), $e);
        }
        
        if ($response->getStatusCode() != 200) {
            $status = $response->getStatusCode();
            throw new WorldCatClientException(
                $response->getReasonPhrase() . " (${status})", $status
            );
        }

        $data = json_decode($response->getBody());
        $this->normalizeLibrariesLookupData($data);

        try {
            $result = $this->jsonMapper->map($data, new LibraryLookupResult);
        } catch (\JsonMapper_Exception $e) {
            throw new WorldCatClientException($e->getMessage(), $e->getCode(), $e);
        }

        return $result;
    }

    /**
     * @param string $oclcn
     *
     * @return LibraryLookupResult
     * @throws WorldCatClientException
     * @throws HttpClientException
     */
    public function lookupLibrariesByOCLCN($oclcn, array $params = [])
    {
        return $this->lookupLibraries("$oclcn", $params);
    }

    /**
     * @param string $isbn
     *
     * @return LibraryLookupResult
     * @throws WorldCatClientException
     * @throws HttpClientException
     */
    public function lookupLibrariesByISBN($isbn, array $params = [])
    {
        return $this->lookupLibraries("isbn/$isbn", $params);
    }

    /**
     * @param string $issn
     *
     * @return LibraryLookupResult
     * @throws WorldCatClientException
     * @throws HttpClientException
     */
    public function lookupLibrariesByISSN($issn, array $params = [])
    {
        return $this->lookupLibraries("issn/$issn", $params);
    }

    /**
     * @param string $sn
     *
     * @return LibraryLookupResult
     * @throws WorldCatClientException
     * @throws HttpClientException
     */
    public function lookupLibrariesBySN($sn, array $params = [])
    {
        return $this->lookupLibraries("sn/$sn", $params);
    }

    /**
     * @param object $data
     * @return void
     */
    protected function normalizeLibrariesLookupData($data)
    {
        $data->title = isset($data->title) ? $data->title : '';
        $data->author = isset($data->author) ? $data->author : '';
        $data->publisher = isset($data->publisher) ? $data->publisher : '';
        $data->oclcNumber = isset($data->OCLCnumber) ? $data->OCLCnumber : '';
        $data->date = isset($data->date) ? $data->date : '';
        $data->isbns = isset($data->ISBN) ? $data->ISBN : [];
        $data->diagnostic = isset($data->diagnostic) ? $data->diagnostic : null;
        $data->libraries = isset($data->library) ? $data->library : [];

        foreach ($data->libraries as $lib) {
            $this->normalizeLibraryData($lib);
        }

        unset($data->OCLCnumber, $data->ISBN, $data->library, $data->diagnostic);
    }

    /**
     * @param object $lib
     * @return void
     */
    protected function normalizeLibraryData($lib)
    {
        $lib->diagnostic = isset($lib->diagnostic) ? $lib->diagnostic : null;
        $lib->institutionName = isset($lib->institutionName) ? $lib->institutionName : '';
        $lib->streetAddress1 = isset($lib->streetAddress1) ? $lib->streetAddress1 : '';
        $lib->streetAddress2 = isset($lib->streetAddress2) ? $lib->streetAddress2 : '';
        $lib->city = isset($lib->city) ? $lib->city : '';
        $lib->state = isset($lib->state) ? $lib->state : '';
        $lib->country = isset($lib->country) ? $lib->country : '';
        $lib->postalCode = isset($lib->postalCode) ? $lib->postalCode : '';
        if (isset($lib->opacUrl))
        {
            try {
                $query_string = parse_url($lib->opacUrl, PHP_URL_QUERY);
                parse_str($query_string, $query_params);
                $lib->opacUrl = urldecode($query_params['url']);
            }
            catch (\Exception $exception) {
                $lib->opacUrl = '';
            }
        }
        else
        {
            $lib->opacUrl = '';
        }
        $lib->oclcSymbol = isset($lib->oclcSymbol) ? $lib->oclcSymbol : '';
        $lib->distance = isset($lib->distance) ? $lib->distance : '';
    }
}
