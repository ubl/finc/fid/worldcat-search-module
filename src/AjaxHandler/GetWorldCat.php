<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
namespace finc\WorldCat\AjaxHandler;

use finc\WorldCat\Client\WorldCatClient;
use Laminas\Config\Config;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Mvc\Controller\Plugin\Params;
use Laminas\View\Renderer\RendererInterface;
use VuFind\Record\Loader;

class GetWorldCat extends \VuFind\AjaxHandler\AbstractBase
{

    /**
     * Top-level configuration
     *
     * @var Config
     */
    protected $config;

    /**
     * HTTP Service
     *
     * @var WorldCatClient
     */
    protected $worldCatClient;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Record loader
     *
     * @var Loader
     */
    protected $recordLoader;

    /**
     * View renderer
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * Constructor
     *
     * @param WorldCatClient    $worldCatClient http client
     * @param Loader            $loader         VuFind Record Loader
     * @param Request           $request        Request
     * @param Config            $config         Top-level configuration
     * @param RendererInterface $renderer       view
     */
    public function __construct(
        WorldCatClient $worldCatClient,
        Loader $loader,
        Request $request,
        Config $config,
        RendererInterface $renderer
    ) {
        $this->worldCatClient = $worldCatClient;
        $this->recordLoader = $loader;
        $this->request = $request;
        $this->config = $config;
        $this->renderer = $renderer;
    }

    /**
     * Handle a request.
     *
     * @param Params $params Parameter helper from controller
     *
     * @return array [response data, HTTP status code]
     */
    public function handleRequest(Params $params)
    {
        try {
            if (!isset($this->config->General->wskey)) {
                return $this->formatResponse(
                    'Configuration file worldcat.ini not found',
                    self::STATUS_HTTP_BAD_REQUEST
                );
            }
            $driver = $this->recordLoader->load(
                $params->fromQuery('id'),
                $params->fromQuery('source')
            );
            $isbn = $driver->getISBNs();
            $oclcn = $driver->getOCLC();
            $userip = $this->request->getServer()->get('HTTP_X_FORWARDED_FOR') != null
                ? $this->request->getServer()->get('HTTP_X_FORWARDED_FOR')
                : $this->request->getServer()->get('REMOTE_ADDR');
            $params = [];
            $params['wskey'] = $this->config->General->wskey;
            $params['ip'] = $userip;
            if ($oclcn) {
                $results = $this->worldCatClient->lookupLibrariesByOCLCN($oclcn[0], $params);
            } elseif ($isbn) {
                $results = $this->worldCatClient->lookupLibrariesByISBN($isbn[0], $params);
            }
            if (isset($results) && empty($results->getFoundLibraries())) {
                $params['ip'] = $this->config->General->serverip;
                if ($oclcn) {
                    $results = $this->worldCatClient->lookupLibrariesByOCLCN($oclcn[0], $params);
                } elseif ($isbn) {
                    $results = $this->worldCatClient->lookupLibrariesByISBN($isbn[0], $params);
                }
            }
            $retval = [];
            $i = 0;
            if (!empty($results) && is_null($results->getDiagnostic())) {
                $results = $results->getLibraries();
                if (!empty($results)) {
                    foreach ($results as $result) {
                        if (is_null($result->getDiagnostic())) {
                            $retval[$i]['name'] = $result->getInstitutionName();
                            $retval[$i]['url'] = $result->getOpacUrl();
                            $i++;
                        }
                    }
                }
            }
            $view = [
                'data' => $retval
            ];
            $html = $this->renderer->render('ajax/worldcat.phtml', $view);
            return $this->formatResponse(compact('html'));
        } catch (\Exception $e) {
            return $this->formatResponse($e->getMessage());
        }
    }
}
